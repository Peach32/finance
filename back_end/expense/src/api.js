var express = require ('express');
var router  = express.Router ();

// Supporting Scripts

router.post ('/create_expense', function (req, res) {
  // Create account object
  // Write to database
  // Return account id
  console.log ("Create Expense");
  let account_id = {"id":"1"};
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(account_id));
  res.end();
});

router.post ('/get_expense', function (req, res) {
  // Query database for account_id
  // Create Account object
  // Return account object
  console.log ("Get Expense");
  let account = {"id":"1",
                 "name":req.body.name};

  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(account));
  res.end();
});

router.post ('/delete_expense', function (req, res) {
  // Query database for account_id
  // Delete Account from database
  // Return status
  console.log ("Delete Expense");
  let account = {"id":"1"};
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(account));
  res.end();
});

module.exports = router;
