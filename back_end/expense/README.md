# Expense

An [Account][1] type that holds an infinite balance but only deposits according  
to the specified [Link][2]. Balance can only flow in, not out.

## API 

Create_Expense
* Inputs:
  * Name
* Outpuits:
  * Expense_Id

Get_Expense
* Inputs:
  * Expense_Id
* Outputs:
  * Expense

Delete_Expense
* Inputs:
  * Expense_Id
* Outputs:
  * N/A

[1]: account
[2]: link
