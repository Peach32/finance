# Income

An [Account][1] type that holds an infinite balance but only withdraws  
according to the specified [Link][2]. Balance can only flow out, not in.

## API

Create_Income
* Inputs:
  * Name
* Outputs:
  * Income_Id

Get_Income
* Inputs:
  * Income_Id
* Outputs:
  * Income

Delete_Income
* Inputs:
  * Income_Id
* Outputs:
  * N/A

[1]: account
[2]: link
