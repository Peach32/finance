# Back_End

These are a collection of the backend services, see the README in each  
service's folder for more info.  

## Framework

```

            +-------------------------------+
            |            Service            |
+------+    | +-----+    +----------------+ |    +----------+
| User |<-->| | API |<-->| Implementation | |<-->| Database |
+------+    | +-----+    +----------------+ |    +----------+
            +-------------------------------+
            
```

Each Service contains its own API and Implementation. These are independent of  
each other and are reflected in the versioning scheme:  
`[API_VERSION].[IMPLEMENTATION_VERSION]`

Any changes made to the API is reflected in the major version number. These  
changes are considered _breaking_, they will impact external users.

Any changes made to the back end implementation is reflected in the minor   
version number. These changes are not necessarily _breaking_ and should not  
impact external users too much but they will need to be aware that a change  
has occured.

Services follow the Func-y API pattern.
See [Func-y API](https://wagslane.dev/posts/func-y-json-api/) for more info.  

All API calls follow the same convention:  
`/[API_VERSION].[IMPLEMENTATION_VERSION]/[SERVICE_NAME]/[FUNCTION_NAME]`

All API calls are POST requests. See Service API documentation for specific  
input / output parameters.

Deprecated API versions are supported for 6 months before being removed from  
the system. Calls to deprecated APIs will include a warning message in the  
response to inform users that an update is required. Unsupported API calls  
will return a message indicating the latest supported version.  

All Implementation versions will be supported throughout the lifecycle of an  
API version.

## Tech Stack

API - NodeJS  
Implementation - Specific to Service  
DB - PostgreSQL  

Everything is hosted on Digital Ocean App Platform.
