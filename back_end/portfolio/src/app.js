// Express App
var express = require ('express');
var app     = express ();
var cors    = require ('cors');

// Require Routes
var api = require('./api.js');

const port = process.env.PORT;

app.use (express.json({type:['application/json', 'text/plain']}));
app.use (express.urlencoded ({ extended: true }));

// Use Routes for pages
app.use ('/', api);

// Start Express App
app.listen (port, localhost => {
  console.log ('Success');
});
