var express = require ('express');
var router  = express.Router ();

// Supporting Scripts

router.post ('/create_portfolio', function (req, res) {
  // Create account object
  // Write to database
  // Return account id
  console.log ("Create Portfolio");
  let portfolio_id = {"id":"1"};
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(portfolio_id));
  res.end();
});

router.post ('/get_portfolio', function (req, res) {
  // Query database for account_id
  // Create Account object
  // Return account object
  console.log ("Get Portfolio");
  let portfolio = {"id":"1",
                 "name":req.body.name};

  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(portfolio));
  res.end();
});

router.post ('/delete_portfolio', function (req, res) {
  // Query database for account_id
  // Delete Account from database
  // Return status
  console.log ("Delete Portfolio");
  let portfolio = {"id":"1"};
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(portfolio));
  res.end();
});

router.post ("add_account', function (req, res) {
  // Query database for account_id
  // Delete Account from database
  // Return status
  console.log ("Add Account");
  let portfolio = {"id":"1"};
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(portfolio));
  res.end();
});

router.post ('/remove_account', function (req, res) {
  // Query database for account_id
  // remove Account from database
  // Return status
  console.log ("remove Account");
  let portfolio = {"id":"1"};
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(portfolio));
  res.end();
});

router.post ('/add_link', function (req, res) {
  // Query database for account_id
  // remove Account from database
  // Return status
  console.log ("Add Link");
  let portfolio = {"id":"1"};
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(portfolio));
  res.end();
});

router.post ('/remove_link', function (req, res) {
  // Query database for account_id
  // remove Account from database
  // Return status
  console.log ("remove Link");
  let portfolio = {"id":"1"};
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(portfolio));
  res.end();
});

router.post ('/update_accounts', function (req, res) {
  // Query database for account_id
  // Delete Account from database
  // Return status
  console.log ("Update Accounts");
  let portfolio = {"id":"1"};
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(portfolio));
  res.end();
});

router.post ('/update_links', function (req, res) {
  // Query database for account_id
  // Delete Account from database
  // Return status
  console.log ("Update Accounts");
  let portfolio = {"id":"1"};
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(portfolio));
  res.end();
});

module.exports = router;
