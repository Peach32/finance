# Portfolio

A portfolio is an overarching collection of [Accounts][1] and their [Links][2].  

## API

### v1.0

Create_Portfolio
* Inputs:
  * Name
* Outputs:
  * Portfolio_Id

Get_Portfolio
* Inputs:
  * Portfolio_Id
* Outputs:
  * Portfolio

Add_Account
* Inputs:
  * Portfolio_Id
  * Account_Id
* Outputs:
  * Portfolio

Add_Link
* Inputs:
  * Portfolio_Id
  * Link_Id
* Outputs:
  * Portfolio

Remove_Account
* Inputs:
  * Portfolio_Id
  * Account_Id
* Outputs:
  * Portfolio

Remove_Link
* Inputs:
  * Portfolio_Id
  * Link_Id
* Outputs:
  * Portfolio

Delete_Portfolio
* Inputs:
  * Portfolio_Id
* Outputs:
 * N/A

[1]: "../account/README.md"
[2]: ../link/README.md
