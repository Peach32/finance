var express = require ('express');
var router  = express.Router ();

// Supporting Scripts

router.post ('/create_account', function (req, res) {
  // Create account object
  // Write to database
  // Return account id
  console.log ("Create Account");
  let account_id = {"id":"1"};
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(account_id));
  res.end();
});

router.post ('/get_account', function (req, res) {
  // Query database for account_id
  // Create Account object
  // Return account object
  console.log ("Get Account");
  let account = {"id":"1",
                 "name":req.body.name,
                 "balance":req.body.balance,
                 "interest":req.body.interest};

  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(account));
  res.end();
});

router.post ('/set_balance', function (req, res) {
  // Query database for account_id
  // Create Account object
  // Return account object
  console.log ("Set Balance");
  let account = {"id":"1",
                 "balance":req.body.balance};
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(account));
  res.end();
});

router.post ('/set_interest', function (req, res) {
  // Query database for account_id
  // Create Account object
  // Update interest rate (APY)
  // Write account to database
  // Return new account object
  console.log ("Set Interest");
  let interest = req.body.interest;
  let account = {"id":"1",
                 "interest":interest};
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(account));
  res.end();
});

router.post ('/set_name', function (req, res) {
  // Query database for account_id
  // Create Account object
  // Update name
  // Write account to database
  // Return new account object
  console.log ("Set Name");
  let account = {"id":"1",
                 "name":req.body.name};
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(account));
  res.end();
});

router.post ('/delete_account', function (req, res) {
  // Query database for account_id
  // Delete Account from database
  // Return status
  console.log ("Delete Account");
  let account = {"id":"1"};
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(account));
  res.end();
});

module.exports = router;
