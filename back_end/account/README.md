# Account

A financial account, carries a balance and may or may not provide interest.  
Accounts can be [linked][1] to other [Accounts][2], [Expenses][3], or [Income][[4].  

## API

Create_Account
* Inputs:
  * Name
  * Balance
  * Interest (APY)
* Outputs:
  * Account_Id

Get_Account
* Inputs:
  * Account_Id
* Outputs:
  * Account

Set_Balance
* Inputs:
  * Account_Id
  * Balance
* Outputs:
  * Account

Set_Interest
* Inputs:
  * Account_Id
  * Interest (APY)
* Outputs:
  * Account

Set_Name
* Inputs:
  * Account_Id
  * Name
* Outputs:
  * Account

Delete_Account
* Inputs:
  * Account_Id
* Outputs:
  * N/A

[1]: finanace/back_end/links/README.md
[2]: account/README.md
[3]: expenses/README.md
[4]: income/README.md
