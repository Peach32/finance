var express = require ('express');
var router  = express.Router ();

// Supporting Scripts

router.post ('/create_link', function (req, res) {
  // Create account object
  // Write to database
  // Return account id
  console.log ("Create Link");
  let link_id = {"id":"1"};
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(link_id));
  res.end();
});

router.post ('/get_link', function (req, res) {
  // Query database for account_id
  // Create Account object
  // Return account object
  console.log ("Get Link");
  let link = {"id":"1",
              "name":req.body.name,
              "from_account":req.body.from_account,
              "to_account":req.body.to_account,
              "amount":req.body.amount,
              "frequency":req.body.frequency};

  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(account));
  res.end();
});

router.post ('/set_to_account', function (req, res) {
  // Query database for account_id
  // Create Account object
  // Return account object
  console.log ("Set To_Account");
  let link = {"id":"1",
              "to_account":req.body.account_id};
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(link));
  res.end();
});

router.post ('/set_from_account', function (req, res) {
  // Query database for account_id
  // Create Account object
  // Update interest rate (APY)
  // Write account to database
  // Return new account object
  console.log ("Set From_Account");
  let from_account = req.body.account_id;
  let link = {"id":"1",
              "from_account":from_account};
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(link));
  res.end();
});

router.post ('/set_amount', function (req, res) {
  // Query database for account_id
  // Create Account object
  // Update name
  // Write account to database
  // Return new account object
  console.log ("Set Amount");
  let link = {"id":"1",
              "amount":req.body.amount};
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(link));
  res.end();
});

router.post ('/set_frequency', function (req, res) {
  // Query database for account_id
  // Create Account object
  // Update name
  // Write account to database
  // Return new account object
  console.log ("Set Frequency");
  let link = {"id":"1",
              "frequency":req.body.frequency};
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(link));
  res.end();
});

router.post ('/delete_link', function (req, res) {
  // Query database for account_id
  // Delete Account from database
  // Return status
  console.log ("Delete Link");
  let link = {"id":"1"};
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(link));
  res.end();
});

module.exports = router;
