# Link

The connection between [Accounts][1], [Income][2], and [Expenses]. These hold  
amount and the frequency of transfers between linked entities.  Responsible for  
ensuring that the transfers happen accurately and on time.

## API

Create_Link
* Inputs:
  * Account_Id (From)
  * Account_Id (To)
  * Amount
  * Frequency
* Outputs:
  * LInk_Id

Get_Link
* Inputs:
  * Link_Id
* Outputs:
  * Link

Set_To_Account
* Inputs:
  * Link_Id
  * Account_Id
* Outputs:
  * Link

Set_From_Account
* Inputs:
  * Link_Id
  * Account_Id
* Outputs:
  * Link

Set_Amount
* Inputs:
  * Link_Id
  * Amount
* Outputs:
  * Link

Set_Frequency
* Inputs:
  * Link_Id
  * Frequency
* Outputs:
  * Link

Delete_Link
*  Inputs:
  * Link_Id
* Outputs:
  * N/A


