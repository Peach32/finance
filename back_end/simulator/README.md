# Simulator

Responsible for loading the [Portfolio][1] and its entities. Manages the timers  
and controls the speed of the simulated run. Updates the states and kicks off  
any events needed.

## API

Create_Simulation
* Inputs:
  * Name
  * Portfolio_Id
* Outputs:
  * Simulation

Delete_Simulation
* Inputs:
  * Simulation_Id
* Outputs:
  * N/A

Step_Simulation
* Inputs:
  * Simulation_Id
  * Time_Step
* Outputs:
  * Simulation

Run_Simulation
* Inputs:
  * Simulation_Id
  * Start_Time
  * Stop_Time
* Outputs:
  * Simulation

Pause_Simulation
* Inputs:
  * Simulation_Id
  * Stop_Time
* Outputs:
  * Simulation

  [1]: portfolio
